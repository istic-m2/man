package fr.istic;

/**
 * Hello world!
 *
 */
public class App 
{
    private int iDoNothing(){
        System.out.println("You'll never see this message");

        int e = 0;
        for(int i=0; i<20; i++){
            e += i;
        }

        for(int i=0; i<20; i++){
            e += i;
        }

        for(int i=0; i<20; i++){
            e += i;
        }
        return e;        
    }

    private int iDoNothing2(){
        System.out.println("You'll never see this message");

        int e = 0;
        for(int i=0; i<20; i++){
            e += i;
        }
        return e;
    }

    private int iDoNothing3(){
        System.out.println("You'll never see this message");

        int e = 0;
        for(int i=0; i<20; i++){
            e += i;
        }
        return e;
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
